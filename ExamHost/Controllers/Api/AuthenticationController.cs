﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using ExamDto.Dto;
using ExamDto.Dto.Account;
using ExamHost.Models.Account;
using ExamHost.Models.Account.Abstract;
using ExamPersistantStore.Entities;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.IdentityModel.Tokens;
using SignInResult = Microsoft.AspNetCore.Identity.SignInResult;

namespace ExamHost.Controllers.Api
{
    [Route("api/[controller]")]
    [ApiController]
    public class AuthenticationController : ControllerBase
    {
        public AuthenticationController(UserManager<ExamUser> userManager, SignInManager<ExamUser> signInManager)
        {
            this.userManager = userManager;
            this.signInManager = signInManager;
        }

        private readonly UserManager<ExamUser> userManager;
        private readonly SignInManager<ExamUser> signInManager;

        [AllowAnonymous]
        [HttpPost]
        public async Task<ActionResult<RequestResult>> PostAsync([FromForm]AuthenticationRequest authRequest, [FromServices] IJwtSigningEncodingKey signingEncodingKey)
        {

            try
            {
                // 1. Проверяем данные пользователя из запроса.
                var result = await signInManager.PasswordSignInAsync(authRequest.Name, authRequest.Password, false, false);
                if (result == SignInResult.Success)
                {
                    // 2. Создаем утверждения для токена.
                    var claims = new Claim[]
                    {
                        new Claim(ClaimTypes.NameIdentifier, authRequest.Name)
                    };
                    // 3. Генерируем JWT.
                    var token = new JwtSecurityToken(
                        issuer: "ExamClientApp",
                        audience: "ExamClient",
                        claims: claims,
                        expires: DateTime.Now.AddMinutes(5),
                        signingCredentials: new SigningCredentials(
                                signingEncodingKey.GetKey(),
                                signingEncodingKey.SigningAlgorithm)
                    );

                    string jwtToken = new JwtSecurityTokenHandler().WriteToken(token);
                    var userDto = new ExamUserDto { Name = authRequest.Name, Token = jwtToken, Rolies = new List<ExamRoleDto>() };
                    var user = await userManager.FindByNameAsync(authRequest.Name);
                    var roles = await userManager.GetRolesAsync(user);
                    foreach(var r in roles)
                    {
                        userDto.Rolies.Add(new ExamRoleDto { Name = r });
                    }
                    return new RequestResult
                    {
                        IsOk = true,
                        Result = userDto
                    };

                }
                else if (result == SignInResult.LockedOut)
                {
                    return new RequestResult 
                    { 
                        IsOk = false,
                        Message = "Пользователь заблокирован.."
                    };
                }
                else if (result == SignInResult.NotAllowed)
                {
                    return new RequestResult
                    {
                        IsOk = false,
                        Message = "Имя пользователя или пароль указаны неверно."
                    };
                }
                return new RequestResult
                {
                    IsOk = false,
                    Message = "Неизвестная ошибка."
                };
            }
            catch (Exception e)
            {
                return new RequestResult
                {
                    IsOk = false,
                    Message = "Возникло исключние на сервере: " + e.Message
                };
            }
        }
    }
}