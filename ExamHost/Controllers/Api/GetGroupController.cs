﻿using ExamDto.Dto;
using ExamPersistantStore.Abstract;
using ExamPersistantStore.Entities;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Linq;
using System.Threading.Tasks;

namespace ExamHost.Controllers.Api
{
    [Route("api/[controller]")]
    [ApiController]
    public class GetGroupController : ControllerBase
    {
        public GetGroupController(IMainRepository repository)
        {
            this.repository = repository;
        }

        private readonly IMainRepository repository;
        [Authorize(Roles = RoleNames.ADMINROLENAME)]
        [HttpPost]
        public async Task<ActionResult<RequestResult>> PostAsync([FromForm]GroupsRequest itemRequest)
        {
            // не стал пока выносить логику из метода контроллера в модель домена (хотя в рабочем варианте это лучше сделать)
            return await Task.Factory.StartNew(() =>
            {
                var first = itemRequest.First;  // для пагинации (пока не задействовано)
                var count = itemRequest.Count;  // для пагинации (пока не задействовано)
                var groups = repository.GroupEnts.Select(p =>
                    new GroupDto
                    {
                        Name = p.Name,
                        Description = p.Description,
                        Icon = p.Icon,
                        GroupEntId = p.GroupEntId,
                        Id = p.Id
                    }).ToList();

                foreach(var i in groups)
                {
                    i.Groups = groups.Where(gr => gr.GroupEntId == i.Id).ToList();
                    i.NumberOfItem = i.Groups.Count;
                    i.Items = repository.ItemEnts.Where(it => it.GroupEntId == i.Id).Select(it =>
                         new ItemDto
                         {
                             Id = it.Id,
                             Name = it.Name,
                             CreationDate = it.CreationDate,
                             Description = it.Description,
                             Icon = it.Icon,
                             IsApproved = it.IsApproved,
                             GroupEntId = it.GroupEntId
                         }).ToList();
                    i.NumberOfItem = i.Items.Count;
                }
                return new RequestResult { IsOk = true, Result = groups.Where(g=>g.GroupEntId == null).ToList() };
            });
        }
    }
}