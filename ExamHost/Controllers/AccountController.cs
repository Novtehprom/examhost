﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ExamDto.Dto.Account;
using ExamPersistantStore.Entities;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;

namespace ExamHost.Controllers
{
    public class AccountController : Controller
    {
        public AccountController(UserManager<ExamUser> userManager, SignInManager<ExamUser> signInManager)
        {
            this.userManager = userManager;
            this.signInManager = signInManager;
        }

        private readonly UserManager<ExamUser> userManager;
        private readonly SignInManager<ExamUser> signInManager;

        public async Task<IActionResult> Initialise()
        {
            var admin = await userManager.FindByNameAsync("Admin");
            if (admin == null)
            {
                var newUser = new ExamUser { UserName = "Admin" };
                var result = await userManager.CreateAsync(newUser, "Admin");
                if (result.Succeeded)
                {
                    await userManager.AddToRoleAsync(newUser, RoleNames.ADMINROLENAME);
                }
                newUser = new ExamUser { UserName = "User" };
                result = await userManager.CreateAsync(newUser, "User");
                if (result.Succeeded)
                {
                    await userManager.AddToRoleAsync(newUser, RoleNames.ADMINROLENAME);
                }

                return RedirectToAction("Index","Home", new { txt = "Добавлены два пользователя" });
            }
            else
            {
                return RedirectToAction("Index", "Home", new { txt = "Инициализация уже выполнена" });
            }
        }
    }
}