﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using ExamHost.Models;
using Microsoft.AspNetCore.Identity;
using ExamPersistantStore.Entities;

namespace ExamHost.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;

        public HomeController(ILogger<HomeController> logger, UserManager<ExamUser> userManager, SignInManager<ExamUser> signInManager)
        {
            _logger = logger;
            this.userManager = userManager;
        }

        private readonly UserManager<ExamUser> userManager;

        public async Task<IActionResult> Index(string txt ="")
        {
            var admin = await userManager.FindByNameAsync("Admin");
            ViewBag.Init = admin == null;
            ViewBag.InitText = txt;
            return View();
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
