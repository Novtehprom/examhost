﻿using ExamHost.Models.Account.Abstract;
using Microsoft.IdentityModel.Tokens;
using System.Text;

namespace ExamHost.Models.Account
{
    public class SigningSymmetricKey: IJwtSigningEncodingKey, IJwtSigningDecodingKey
    {
        private readonly SymmetricSecurityKey secretKey;

        public string SigningAlgorithm { get; } = SecurityAlgorithms.HmacSha256;

        public SigningSymmetricKey(string key)
        {
            this.secretKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(key));
        }

        public SecurityKey GetKey() => this.secretKey;
    }
}
