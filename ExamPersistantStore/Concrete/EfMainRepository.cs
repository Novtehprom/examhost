﻿using ExamPersistantStore.Abstract;
using ExamPersistantStore.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ExamPersistantStore.Concrete
{
    public class EfMainRepository : IMainRepository
    {

        public EfMainRepository(ExamDbContext context)
        {
            this.context = context;
        }
        protected ExamDbContext context;

        public IQueryable<GroupEnt> GroupEnts => context.GroupEnts;

        public IQueryable<ItemEnt> ItemEnts => context.ItemEnts;
    }
}
