﻿using ExamPersistantStore.Entities;
using ExamPersistantStore.Entities.AuthBase;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ExamPersistantStore.Concrete
{
    public class DbInitializer
    {
        public static void Initialize(ExamDbContext context)
        {
            if (!context.Roles.Any())
            {
                context.Roles.Add(new RoleIntPk { Name = RoleNames.ADMINROLENAME, NormalizedName = RoleNames.ADMINROLENAME.ToUpper() });
                context.Roles.Add(new RoleIntPk { Name = RoleNames.USERROLENAME, NormalizedName = RoleNames.USERROLENAME.ToUpper() });
                context.SaveChanges();
            }
        }
    }
}
