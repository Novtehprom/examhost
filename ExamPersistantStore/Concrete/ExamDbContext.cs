﻿using ExamPersistantStore.Entities;
using ExamPersistantStore.Entities.AuthBase;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace ExamPersistantStore.Concrete
{
    public class ExamDbContext: IdentityDbContext<ExamUser, RoleIntPk, int, UserClaimIntPk, UserRoleIntPk, UserLoginIntPk, RoleClaimIntPk, UserTokenIntPk>
    {
        public ExamDbContext(DbContextOptions<ExamDbContext> options): base(options)
        {
            Database.EnsureCreated();
        }

        public DbSet<GroupEnt> GroupEnts { get; set; }
        public DbSet<ItemEnt> ItemEnts { get; set; }


        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            //modelBuilder.Entity<ExamUser>().HasData(new ExamUser { Id = 1, UserName = "Admin", Email = "admin@amin.ru" });
        }
    }
}

