﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ExamPersistantStore.Entities
{
    public static class RoleNames
    {
        public const string ADMINROLENAME = "Админиcтратор";
        public const string USERROLENAME = "Пользователь сайта";
    }
}
