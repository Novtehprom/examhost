﻿using ExamPersistantStore.Entities.Base;
using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace ExamPersistantStore.Entities
{
    public class GroupEnt: BaseEnt
    {
        public int? GroupEntId { get; set; }
        public virtual GroupEnt ParentGroupEnt { get; set; }
        public byte[] Icon { get; set; }
        [MaxLength(64)]
        public string Name { get; set; }
        [MaxLength(512)]
        public string Description { get; set; }
        public virtual IQueryable<GroupEnt> GroupEnts { get; set; }
        public virtual IQueryable<ItemEnt> ItemEnts { get; set; }
    }
}
