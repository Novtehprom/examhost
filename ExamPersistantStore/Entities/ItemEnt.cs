﻿using ExamPersistantStore.Entities.Base;
using System;
using System.ComponentModel.DataAnnotations;

namespace ExamPersistantStore.Entities
{
    public class ItemEnt: BaseEnt
    {
        public int GroupEntId { get; set; }
        public virtual GroupEnt ParentGroupEnt { get; set; }
        public byte[] Icon { get; set; }
        [MaxLength(64)]
        public string Name { get; set; }
        [MaxLength(512)]
        public string Description { get; set; }
        public DateTime CreationDate { get; set; }
        public bool? IsApproved { get; set; }
    }
}
