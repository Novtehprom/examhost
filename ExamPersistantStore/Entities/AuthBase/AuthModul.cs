﻿using System;
using System.Collections.Generic;
using System.Text;
using ExamPersistantStore.Concrete;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;


namespace ExamPersistantStore.Entities.AuthBase
{
    public class UserRoleIntPk : IdentityUserRole<int>
    {
    }

    public class UserClaimIntPk : IdentityUserClaim<int>
    {
    }
    public class RoleClaimIntPk : IdentityRoleClaim<int>
    {
    }

    public class UserLoginIntPk : IdentityUserLogin<int>
    {
    }

    public class RoleIntPk : IdentityRole<int>
    {
        public RoleIntPk() { }
        public RoleIntPk(string name) { Name = name; }
    }

    public class UserTokenIntPk : IdentityUserToken<int>
    {

    }

    public class UserStoreIntPk : UserStore<ExamUser, RoleIntPk, ExamDbContext, int, UserClaimIntPk, UserRoleIntPk, UserLoginIntPk, UserTokenIntPk, RoleClaimIntPk>
    {
        public UserStoreIntPk(ExamDbContext context)
            : base(context)
        {
        }
    }

    public class RoleStoreIntPk : RoleStore<RoleIntPk, ExamDbContext, int, UserRoleIntPk, RoleClaimIntPk>
    {
        public RoleStoreIntPk(ExamDbContext context)
            : base(context)
        {
        }
    }
}
