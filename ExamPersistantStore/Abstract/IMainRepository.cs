﻿using ExamPersistantStore.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ExamPersistantStore.Abstract
{
    public interface IMainRepository
    {
        IQueryable<GroupEnt> GroupEnts { get; }
        IQueryable<ItemEnt> ItemEnts { get; }
    }
}
