﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace ExamPersistantStore.Migrations
{
    public partial class datetime : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "NumberOfDaughter",
                table: "GroupEnts");

            migrationBuilder.AlterColumn<DateTime>(
                name: "CreationDate",
                table: "ItemEnts",
                nullable: false,
                oldClrType: typeof(int),
                oldType: "int");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<int>(
                name: "CreationDate",
                table: "ItemEnts",
                type: "int",
                nullable: false,
                oldClrType: typeof(DateTime));

            migrationBuilder.AddColumn<int>(
                name: "NumberOfDaughter",
                table: "GroupEnts",
                type: "int",
                nullable: false,
                defaultValue: 0);
        }
    }
}
